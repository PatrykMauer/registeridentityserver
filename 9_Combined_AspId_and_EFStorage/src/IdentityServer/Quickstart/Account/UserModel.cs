using System.ComponentModel.DataAnnotations;

namespace IdentityServer.Quickstart.Account
{
    public class UserModel
    {
        [Required] 
        public string Username;
        [Required]
        public string Password;
    }
}
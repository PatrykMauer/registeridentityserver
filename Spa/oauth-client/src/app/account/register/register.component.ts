import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators'
import { AuthService } from '../../core/authentication/auth.service';
import { UserRegistration }    from '../../shared/models/user.registration';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  success: boolean;
  error: string;
  userRegistration: UserRegistration = { name: '', email: '', password: ''};
  submitted: boolean = false;

  constructor(private authService: AuthService, private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
  }

  onSubmit() {

    this.spinner.show();  //kolko ladowania

    this.authService.register(this.userRegistration) //register wywołuje POST usera
      .pipe(finalize(() => { // finalize Call a function when observable completes or errors. You can use pipes to link operators together. Pipes let you combine multiple functions into a single function. The pipe() function takes as its arguments the functions you want to combine, and returns a new function that, when executed, runs the composed functions in sequence.
        this.spinner.hide();
      }))
      .subscribe(  //Subscribe to begin listening for async result
        result => {  //next - w tej labdzie są funckje, które mają się wykonać po każdej odebranej przez Observable wartości
         if(result) {
           this.success = true;
         }
      },
      error => { // This is a second argument of subscribe
        this.error = error;
      });

    // here could be the third argument which would be complete(), which invokes after an observable is done
  }
}
